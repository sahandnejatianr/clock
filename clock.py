from math import *
import matplotlib.pyplot as plt
import datetime
import matplotlib.animation as animation
#===========================================================
cxy = 20
circle_r = 17
marker_r = 15
big_r = 12
small_r = 6

fig = plt.figure()
ax = fig.add_subplot(111)
#===========================================================
def marker():
    x = []
    y = []
    for i in range(0,360,30):
        x.append(cxy + marker_r*cos(radians(i)))
        y.append(cxy + marker_r*sin(radians(i)))
    return x,y
#===========================================================
def calculate():
    now = datetime.datetime.now()
    s = now.second
    m = now.minute
    h = now.hour
    
    deg_s = (15 - s) * 3 * 2
    xs = [cxy , cxy + (big_r * cos(radians(deg_s)))]
    ys = [cxy , cxy + (big_r * sin(radians(deg_s)))]

    deg_m = (15 - m) * 6
    xm = [cxy , cxy + (big_r * cos(radians(deg_m)))]
    ym = [cxy , cxy + (big_r * sin(radians(deg_m)))]

    deg_h = ((3 - (h % 12)) * 30) - (m / 2)
    xh = [cxy , cxy + (small_r * cos(radians(deg_h)))]
    yh = [cxy , cxy + (small_r * sin(radians(deg_h)))]

    return [[xh,yh],[xm,ym],[xs,ys]]
#===========================================================
def motion(i):
    res = calculate()

    ax.clear()
    ax.plot(res[2][0] , res[2][1] , color = 'r' , linestyle = '--')
    ax.plot(res[1][0] , res[1][1] , color = 'b')
    ax.plot(res[0][0] , res[0][1] , color = 'b')

    x,y = marker()
    ax.plot(x,y , color='b' , linestyle=' ' , marker='D')

    ax.add_artist(plt.Circle((cxy,cxy) , circle_r , fill=False))
    ax.set_aspect("equal")
    ax.axis([0,40,0,40])
#===========================================================

anime = animation.FuncAnimation(fig , motion , interval=500)
plt.show()
